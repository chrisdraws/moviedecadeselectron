import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieHolderComponent } from './movieHolder/movieHolder.component';

const routes: Routes = [
  { path: '', component: MovieHolderComponent },
  { path: 'character/:id', component: MovieHolderComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
