import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient) {}

  readonly apiKey = 'f7099c06';

  getMovies(movieName) {
    return this.http.get('http://www.omdbapi.com/?s=' + movieName + '&apikey=' + this.apiKey);
  }

  getDetails(movieID) {
    return this.http.get('http://www.omdbapi.com/?i=' + movieID + '&apikey=' + this.apiKey);
  }
}
