import { Component, Input, SimpleChanges } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movie-holder',
  templateUrl: './movieHolder.component.html',
  styleUrls: ['./movieHolder.component.scss']
})
export class MovieHolderComponent {
  movies: any[];
  moviesObjects: any[];
  moviesObjectsOriginal: any[];
  sortableDecades: any[] = [
    { class: 'first', text: "2000's", selected: false },
    { class: 'second', text: "1990's", selected: false }
  ];

  @Input()
  movie: string = 'batman';

  constructor(private data: DataService, private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.movie = params.id || 'batman';
      this.data.getMovies(this.movie).subscribe(data => {
        this.handleResponse(data);
      });
    });
  }

  handleResponse(data) {
    const mainObject = this;
    if (data.Response === 'True') {
      mainObject.movies = data.Search;
      mainObject.moviesObjects = [];
      mainObject.moviesObjectsOriginal = [];
      mainObject.movies.forEach(movie => {
        mainObject.data.getDetails(movie.imdbID).subscribe(data => {
          mainObject.moviesObjects.push(data);
          mainObject.moviesObjectsOriginal.push(data);
        });
      });
    } else {
      console.error('Data.Response is false', data);
    }
  }

  onClickSort(event, arrayPos) {
    let filter = false;
    this.sortableDecades = this.sortableDecades.map((decade, index) => {
      return { ...decade, selected: arrayPos === index ? !decade.selected : false };
    });
    this.sortableDecades.forEach(decade => {
      if (decade.selected) {
        filter = true;
      }
    });

    this.moviesObjects = filter
      ? this.moviesObjectsOriginal.filter(movie => {
          return movie.Released.split(' ')[2][0] === (arrayPos === 0 ? '2' : '1');
        })
      : this.moviesObjectsOriginal;
  }
}
