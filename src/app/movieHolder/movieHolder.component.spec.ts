import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MovieHolderComponent } from './movieHolder.component';

describe('MovieHolderComponent', () => {
  let component: MovieHolderComponent;
  let fixture: ComponentFixture<MovieHolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ActivatedRoute, MovieHolderComponent, DataService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieHolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
