import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {
  appTitle: string = 'Movie Decade Selectron';
  categories: Array<any> = [{ title: 'Batman', name: 'batman' }, { title: 'Superman', name: 'superman' }];

  constructor() {}
}
